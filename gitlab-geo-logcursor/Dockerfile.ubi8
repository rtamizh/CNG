ARG RAILS_IMAGE=

# Use a composite image to set the permissions properly in the artifacts. This
# is needed due to two limitations with Docker:
# 1. ADD doesn't set permissions for tarballs: https://github.com/moby/moby/issues/35525
# 2. docker cp doesn't preserve UID/GID: https://github.com/moby/moby/issues/41727
FROM ${RAILS_IMAGE} as composite

ARG UID=1000

ADD gitlab-rails-bootsnap-ee.tar.gz /assets
RUN chown -R ${UID}:0 /assets/srv/gitlab

## FINAL IMAGE ##

ARG RAILS_IMAGE

FROM ${RAILS_IMAGE}

ARG GITLAB_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-geo-logcursor" \
      name="GitLab Geo Log Cursor" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Geo Log Cursor daemon." \
      description="Geo Log Cursor daemon."

# Control the target for `wait-for-deps` schema check
ENV DB_SCHEMA_TARGET=geo

COPY scripts/  /scripts/
COPY --from=composite /assets /

RUN microdnf ${DNF_OPTS} install --best --assumeyes --setopt=install_weak_deps=0 --nodocs procps \
    && microdnf clean all \
    && chown -R ${UID}:0 /scripts /home/${GITLAB_USER} \
    && chmod -R g=u scripts /home/${GITLAB_USER}

USER ${UID}

CMD ["/scripts/process-wrapper"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
